import './App.css'
import ComponenteDos from './Components/ComponenteDos'
import ComponenteUno from './Components/ComponenteUno'
import ComponenteTres from './Components/ComponenteTres'


function App() {

  return (
    <div className="App">

      <ComponenteUno />
      <ComponenteDos />
      <ComponenteTres /> 

    </div>
  )
}

export default App
